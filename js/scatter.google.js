function drawScatterPlot(points) {
    // Create and populate the data table.
    var data = new google.visualization.DataTable();
    data.addColumn('number', 'PartisanScore');
    data.addColumn('number', 'SentimentScore');
    for (var i = 0; i < 100; ++i) {
      data.addRow([ points[i][0], points[i][1]+Math.random()*.8-.4 ])
    }

    // Create and draw the visualization.
        chart.draw(data, {title: 'Tweet Sentiment Spectrum',
                            width: 800, height: 600,
                            colors: ['rgba(223, 83, 83, .05)'],
                            vAxis: {title: "SentimentScore (ss)", titleTextStyle: {color: "black"}},
                            hAxis: {title: "PartisanScore (ps)", titleTextStyle: {color: "black"}},
                            legend: "none",
                            pointSize: 7
                        }
              );
}
