var rangeArgs="";
var options = {
      chart: {
         renderTo: 'container', 
         defaultSeriesType: 'scatter',
         zoomType: 'xy',
         events: {
            selection: updateClouds
        }
      },
      title: {
         text: 'Tweet Sentiment Spectrum'
      },
      subtitle: {
         text: 'Source: SocialCaucus'
      },
      xAxis: {
         title: {
            enabled: true,
            text: 'SentimentScore (ss)'
         },
         startOnTick: true,
         endOnTick: true,
         showLastLabel: true
      },
      yAxis: {
         title: {
            text: 'PartisanScore (ps)'
         }
      },
      tooltip: {
         formatter: function() {
                   return ''+ 
               this.x +' ss, '+ this.y +' ps';
         }
      },
      legend: {
         layout: 'vertical',
         align: 'left',
         verticalAlign: 'top',
         x: 100,
         y: 70,
         floating: true,
         //backgroundColor: Highcharts.theme.legendBackgroundColor || '#FFFFFF',
         backgroundColor: '#FFFFFF',
         borderWidth: 1
      },
      plotOptions: {
         scatter: {
            marker: {
               states: {
                  hover: {
                     enabled: false,
                     lineColor: 'rgb(100,100,100)'
                  }
               }
            },
            states: {
               hover: {
                  marker: {
                     enabled: false
                  }
               }
            }
         }
      },
      series: [{
         name: 'tweets',
         color: 'rgba(223, 83, 83, .1)',
      }]
};


function updateClouds( evt ){
    if (evt.resetSelection == true){
        rangeArgs = "";
    }else{
        xst = evt.xAxis[0].min;
        xen = evt.xAxis[0].max;
        yst = evt.yAxis[0].min;
        yen = evt.yAxis[0].max;
        rangeArgs = "&minx="+xst +"&maxx="+xen +"&miny="+yst +"&maxy="+yen; 
    }
    $.ajax({    type: "GET",
                url: "index.py?page=fetch_points"+rangeArgs,
                success: function(data) {
                     var pts = renderClouds( data );
                     chart.xAxis[0].setExtremes();
                     chart.series[0].setData( pts ); 
                     // fix the reset zoom button
                     $('.highcharts-toolbar').click(resetZoom);
                },
                error: function(req, text, error) {
                    $("#err").html("Reload error!");
                }
            });
    updateTable();
    updateWordCloud();

}

function resetZoom(){
    $.ajax({    type: "GET",
                url: "index.py?page=fetch_points",
                success: function(data) {
                     chart.toolbar.remove('zoom');
                     var pts = renderClouds( data );
                     chart.xAxis[0].setExtremes();
                     chart.series[0].setData( pts ); 
                     // fix the reset zoom button
                     $('.highcharts-toolbar').click(resetZoom);
                },
                error: function(req, text, error) {
                    $("#err").html("Reload error!");
                }
            });
}


function renderClouds(points){

    var markers = new Array();
    var min_pt = 2;
    for (var i=0; i < points.length; i++){
        if (points[i][2] < min_pt){
            min_pt = points[i][2];
        }
    }
    for (var i=0; i < points.length; i++){
        markers[i] = {  "x": points[i][0],
                        "y": points[i][1],
                        "marker": { "radius": Math.log(points[i][2]/min_pt+1) * 10 }
                     };
    }
    return( markers );
}
        

function plotPoints( points ){
    pts = renderClouds( points );
    options.series[0].data = pts;
    chart = new Highcharts.Chart(options);
}



function updateTable( ){
    jQuery.getJSON("index.py?page=fetch_texts"+rangeArgs, function(textdata){
            $("#texts_table").dataTable({
                "bProcessing": true,
                "bDestroy": true,
                "aaData": textdata,
                "columnWidths": "790px"
            });

    });  

}

function updateWordCloud(){
    var mode = $("#text_cloud_mode").val();
    jQuery.getJSON("index.py?page=fetch_counts&mode="+mode+rangeArgs, function(countdata){
    
        //load up the list-items
        var max_idx = 20;
        if (countdata.length < max_idx){
            max_idx = countdata.length;
        }
        var max_wgt = 0;
        for( var i=0; i < max_idx; i++){
                max_wgt += countdata[i][1];
        }
        var content = "<canvas id=tag_cloud name=tagcloud width=600 height=400 style=\"float: left; margin-bottom: 20px\"><ul>";
        for( var i=0; i < max_idx; i++){
            wgt = (countdata[i][1] / max_wgt) * 36 + 4;
            content = content + "<li><a href=# data-weight="+wgt+">"+countdata[i][0]+"</a></li>";
        }
        content = content + "</ul></canvas>";
        $('#text_cloud').html( content );

        $('#tag_cloud').tagcanvas({
             outlineThickness : 1,
             maxSpeed : 0.01,
             depth : 0.75,
             weightFrom: "data-weight",
             weightMode: "both",
             weightSize: 5,
             weight: true
           });                    
    });  
}
