var linelength = 150;

function person(name, gender, lifespan, place) {
	this.x = 100;
	this.y = 100;
	this.name = name;
	this.gender = gender;
	this.lifespan = lifespan;
	this.place = place;
	this.spouse = null;

	this.draw = function() {
		var textoffset = this.x + 10;

		// First, draw the baseline
		this.context.strokeStyle = "#111";
		this.context.beginPath();
		this.context.lineTo(this.x, this.y);
		this.context.lineTo(this.x + linelength, this.y);
		this.context.stroke();

		if (this.spouse) {
			// And draw the marriage line
			this.context.beginPath();
			this.context.lineTo(this.x, this.y);
			this.context.lineTo(this.spouse.x, this.spouse.y);
			this.context.stroke();
		}

		// Now draw the name
		this.context.font = "bold 14px Arial";
		this.context.textAlign = "left";
		this.context.fillStyle = "#000";
		this.context.fillText(this.name, textoffset, this.y - 7);

		// And the birth info
		this.context.font = "11px Arial";
		this.context.fillStyle = "#999";
		this.context.fillText(this.lifespan, textoffset, this.y + 15);
		this.context.fillText(this.place, textoffset, this.y + 28);
	}

	this.isFatherOf = function(child, gen) {
		this.x = child.x + linelength;
		this.y = child.y - (250 / gen);
	}

	this.isMotherOf = function(child, gen) {
		this.x = child.x + linelength;
		this.y = child.y + (250 / gen);
	}

	this.isMarriedTo = function(spouse) {
		this.spouse = spouse;
	}
}

function initFamily(child, father, mother, gen) {
	father.isFatherOf(child, gen);
	mother.isMotherOf(child, gen);
	father.isMarriedTo(mother);
}

var people = new Array();
people.push(new person("George", "m", "1862â€“1906", "Baltimore, MD"),
	new person("Thomas", "m", "1831â€“1884", "Baltimore, MD"),
	new person("Lily", "f", "1826â€“1866", "Brunswick/Baltimore, MD"),
	new person("Peter", "m", "1781â€“1871", "Baltimore, MD"),
	new person("Jane", "f", "1795â€“1871", "Baltimore, MD"),
	new person("Henry", "m", "1792â€“1845", "Montgomery, NC"),
	new person("Sarah", "f", "1793â€“1882", "Montgomery, NC")
);

$(document).ready(function() {
	// Prepare the canvas
	var canvas = document.getElementById("chart");

	// Get context and let the game know about it
	if (canvas.getContext) {
		var context = canvas.getContext('2d');

		// Iniitalize context
		for (var p in people) { people[p].context = context; }

		// Base point
		people[0].x = 150;
		people[0].y = 250;

		initFamily(people[0], people[1], people[2], 2);
		initFamily(people[1], people[3], people[4], 3);
		initFamily(people[2], people[5], people[6], 3);

		for (var p in people) { people[p].draw(); }
	}
});

