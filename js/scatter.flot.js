var options = {
    legend: { show: false },
    series: {
        lines: { show: true },
        points: { show: true }
    },
    yaxis: { ticks: 10 },
    selection: { mode: "xy" }
};

var plotdata=[];
function plotFlot( points ){
        
        plotdata = [{label:"tweets", data: points}];
        var plot = $.plot($("#placeholder"), plotdata, options);
        
        // setup overview
        var overview = $.plot($("#overview"), plotdata, {
            legend: { show: true, container: $("#overviewLegend") },
            series: {
                points: { show: true },
            },
            //xaxis: { ticks: 4 },
            //yaxis: { ticks: 3, min: -2, max: 2 },
            grid: { color: "#999" },
            selection: { mode: "xy" }
        });

        // now connect the two

        $("#placeholder").bind("plotselected", function (event, ranges) {
            // clamp the zooming to prevent eternal zoom
            if (ranges.xaxis.to - ranges.xaxis.from < 0.00001)
                ranges.xaxis.to = ranges.xaxis.from + 0.00001;
            if (ranges.yaxis.to - ranges.yaxis.from < 0.00001)
                ranges.yaxis.to = ranges.yaxis.from + 0.00001;

        // do the zooming
            plot = $.plot($("#placeholder"), getData(ranges.xaxis.from, ranges.xaxis.to),
                          $.extend(true, {}, options, {
                              xaxis: { min: ranges.xaxis.from, max: ranges.xaxis.to },
                              yaxis: { min: ranges.yaxis.from, max: ranges.yaxis.to }
                        }));

            // don't fire event on the overview to prevent eternal loop
            overview.setSelection(ranges, true);
        });
        $("#overview").bind("plotselected", function (event, ranges) {
            plot.setSelection(ranges);
        });

}
