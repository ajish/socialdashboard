#!/usr/bin/env python
import json
import random
import re
from mako.template import Template
from mako.lookup import TemplateLookup
from mako import exceptions


import sys
import os
import time
import cgi

import cgitb
cgitb.enable()


# CGI header

def get_required_var(var,form):
    if not form.has_key(var):
        raise Exception("Missing required CGI parameter: %s" % var)
    return form.getvalue(var)

def get_optional_var(var,form):
    if not form.has_key(var):
        return False
    return form.getvalue(var)

def serve_template(templatename, **kwargs):
    mytemplate = mylookup.get_template(templatename)
    print mytemplate.render(**kwargs)


form = cgi.FieldStorage()

page = get_required_var("page",form)

mylookup = TemplateLookup(directories=['./tmpl'])

unbuffered = os.fdopen(sys.stdout.fileno(), 'w', 0)
sys.stdout = unbuffered

def get_rows( filename ):
    ''' placeholder for fetching the data '''
    lines = []
    for line in open(filename, 'r'):
        lines.append( line.strip().split(",") )
    return lines

def fetch_points( minx=None, maxx=None, miny=None, maxy=None ):
    ''' get the points within the specified range
        identified by tweet-id
    '''
    rows = get_rows( "dat.txt"  )
    ycol = -1
    xcol = -2
    textcol = -3
    idcol = -4
    points = []
    ids = []
    snames = []
    texts = []
    for r in rows:
        x= float(r[xcol])
        y= float(r[ycol])+ random.random()*.8-.4
        y= round(y,3)
        if minx and x < minx:
            continue
        if maxx and x > maxx:
            continue
        if miny and y < miny:
            continue
        if maxy and y > maxy:
            continue
        points.append( [x,y] )
    return points

def get_min_max( points ):
    xmin = 50
    ymin = 50
    xmax = -50
    ymax = -50
    for p in points:
        if p[0] < xmin:
            xmin = p[0]
        if p[0] > xmax:
            xmax = p[0]
        if p[1] < ymin:
            ymin = p[1]
        if p[1] > ymax:
            ymax = p[1]
    return ( xmin, xmax, ymin, ymax )

def normalize_points( points, minx, maxx, miny, maxy ):
    ''' normalize the points within a grid to be a maximum of 100 X 100
        and size the points according to the density
    '''
    nsteps = 100.0
    xstep = (maxx-minx)/nsteps
    ystep = (maxy-miny)/nsteps
    normed = {}
    for p in points:
        xn = (int(p[0]/xstep)) * xstep
        yn = (int(p[1]/ystep)) * ystep
        key = (xn,yn)
        if not normed.has_key( key ):
            normed[key]=1
        else:
            normed[key]+=1
    out = []
    for k,v in normed.items():
        out.append( [k[0], k[1], float(v)/float(len(points))] )
    return out
 
def fetch_texts( minx=None, maxx=None, miny=None, maxy=None ):
    ''' get the tweets within the specified range
        identified by tweet-id
    '''
    rows = get_rows( "dat.txt"  )
    ycol = -1
    xcol = -2
    textcol = -3
    idcol = -4
    texts = []
    for r in rows:
        x= float(r[xcol])
        y= float(r[ycol])
        y= round(y,3)
        if minx and x < minx:
            continue
        if maxx and x > maxx:
            continue
        if miny and y < miny:
            continue
        if maxy and y > maxy:
            continue
        texts.append( [ "%s: %s (%s)" % (r[0], r[textcol], r[idcol]) ] )
    return texts

def fetch_entity_counts( minx=None, maxx=None, miny=None, maxy=None ):
    ''' get the counts within the specified range
        for each entity (twitter-id/hash-tag) found
    '''
    rows = get_rows( "dat.txt"  )
    ycol = -1
    xcol = -2
    textcol = -3
    idcol = -4
    counts = {}
    for r in rows:
        x= float(r[xcol])
        y= float(r[ycol])
        y= round(y,3)
        key = r[0].replace("\"","")
        if minx and x < minx:
            continue
        if maxx and x > maxx:
            continue
        if miny and y < miny:
            continue
        if maxy and y > maxy:
            continue
        txt = r[textcol].lower()
        txt = re.sub(r'[\"|:|,|.|!]',"",txt)
        for key in txt.split():
            if key.startswith("@") or key.startswith("#"):
                if counts.has_key(key):
                    counts[key] += 1
                else:
                    counts[key] = 1
    pairs = []
    for key, value in sorted(counts.iteritems(), key=lambda (k,v): (v,k), reverse=True):
        pairs.append( [key, value] )
    return pairs
def fetch_keyword_counts( minx=None, maxx=None, miny=None, maxy=None ):
    ''' get the counts within the specified range
        for each keyword tracked
    '''
    rows = get_rows( "dat.txt"  )
    ycol = -1
    xcol = -2
    textcol = -3
    idcol = -4
    counts = {}
    for r in rows:
        x= float(r[xcol])
        y= float(r[ycol])
        y= round(y,3)
        key = r[0].replace("\"","")
        if minx and x < minx:
            continue
        if maxx and x > maxx:
            continue
        if miny and y < miny:
            continue
        if maxy and y > maxy:
            continue
        if key == "NULL":
            continue
        if counts.has_key(key):
            counts[key] += 1
        else:
            counts[key] = 1
    pairs = []
    for key, value in sorted(counts.iteritems(), key=lambda (k,v): (v,k), reverse=True):
        pairs.append( [key, value] )
    return pairs

allowed_modes = ["keyword","entity"]

if page == "fetch_points":
    minx = get_optional_var("minx", form)
    maxx = get_optional_var("maxx", form)
    miny = get_optional_var("miny", form)
    maxy = get_optional_var("maxy", form)
    points = fetch_points( float(minx), float(maxx), float(miny), float(maxy) )
    if not minx:
        ranges = get_min_max( points )
    else:
        ranges = (float(minx), float(maxx), float(miny), float(maxy)) 
    points = normalize_points( points, ranges[0], ranges[1], ranges[2], ranges[3] )
    out = json.dumps(points, encoding='latin1')
    print "Content-type: application/json\n\n"
    print out    
elif page == "fetch_texts":
    minx = get_optional_var("minx", form)
    maxx = get_optional_var("maxx", form)
    miny = get_optional_var("miny", form)
    maxy = get_optional_var("maxy", form)
    points = fetch_texts( float(minx), float(maxx), float(miny), float(maxy) )
    out = json.dumps(points, encoding='latin1')
    print "Content-type: application/json\n\n"
    print out    
elif page == "fetch_counts":
    mode = get_required_var("mode", form)
    minx = get_optional_var("minx", form)
    maxx = get_optional_var("maxx", form)
    miny = get_optional_var("miny", form)
    maxy = get_optional_var("maxy", form)
    if mode not in allowed_modes:
        raise Exception("Mode must be one of: %s" % (",".join(allowed_modes)))
    if mode == "keyword":
        counts = fetch_keyword_counts( float(minx), float(maxx), float(miny), float(maxy) )
    elif mode == "entity":
        counts = fetch_entity_counts( float(minx), float(maxx), float(miny), float(maxy) )
        
    out = json.dumps(counts, encoding='latin1')
    print "Content-type: application/json\n\n"
    print out    
else:
    raise Exception("Unsupported request for page: %s" % page)
