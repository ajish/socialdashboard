================
 SocialDashboard
================

Allow filtering tweet clouds by conservative/liberal bias and positive/negative sentiment.
Aggregate out the main keywords and overrepressented twitter entities (users, hashtags)
within a selected block as a wordcloud.
Show the individual tweets and dates for a given selection.
Find a demo `here 
<http://biobits.net/socialdashboard/>`_.
